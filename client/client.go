package client

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net"
	"strconv"
	"strings"
	"time"

	"redis-check/common"

	"reflect"

	"github.com/garyburd/redigo/redis"
	redigoCluster "github.com/vinllen/redis-go-cluster"
)

var (
	emptyError = errors.New("empty")
)

type RedisHost struct {
	Addr         []string
	Password     string
	TimeoutMs    uint64
	Role         string // "source" or "target"
	Authtype     string // "auth" or "adminauth"
	DBType       int
	Prefix       []byte
	DBFilterList map[int]struct{} // whitelist
}

type ScanOpts struct {
	Index      int
	PhysicalDB string
	Match      string
	Count      int
	KeyList    []string
}

func (p RedisHost) String() string {
	return fmt.Sprintf("%s redis addr: %s", p.Role, p.Addr)
}

func (p RedisHost) IsCluster() bool {
	return p.DBType == common.TypeCluster
}

type RedisClient struct {
	redisHost RedisHost
	Db        int32
	conn      redis.Conn
}

func (p RedisClient) String() string {
	if len(p.redisHost.Addr) > 0 {
		return p.redisHost.Addr[0]
	}
	return p.redisHost.String()
}

func NewRedisClient(redisHost RedisHost, db int32) (RedisClient, error) {
	rc := RedisClient{
		redisHost: redisHost,
		Db:        db,
	}
	// send ping command first
	ret, err := rc.Do("ping")
	if err != nil {
		return rc, err
	} else if value, ok := ret.(redis.Error); ok {
		return RedisClient{}, fmt.Errorf("redis error: %v", value.Error())
	} else if value, ok := ret.(string); !ok || value != "PONG" {
		return RedisClient{}, fmt.Errorf("ping return invalid: %v", ret)
	} else {
		return rc, err
	}
}

func (p *RedisClient) CheckHandleNetError(err error) bool {
	netError := false
	if err == io.EOF { // 对方断开网络
		netError = true
	} else if _, ok := err.(net.Error); ok {
		netError = true
	} else if strings.Contains(err.Error(), "broken pipe") {
		netError = true
	}
	if netError && p.conn != nil {
		p.conn.Close()
		p.conn = nil
		time.Sleep(time.Second) // 网络相关错误1秒后重试
	}
	return netError
}

func (p *RedisClient) Connect() error {
	if p.conn != nil {
		return nil
	}

	var err error
	if p.redisHost.IsCluster() == false {
		// single db or proxy
		if p.redisHost.TimeoutMs == 0 {
			p.conn, err = redis.Dial("tcp", p.redisHost.Addr[0])
		} else {
			p.conn, err = redis.DialTimeout("tcp", p.redisHost.Addr[0], time.Millisecond*time.Duration(p.redisHost.TimeoutMs),
				time.Millisecond*time.Duration(p.redisHost.TimeoutMs), time.Millisecond*time.Duration(p.redisHost.TimeoutMs))
		}
	} else {
		// cluster
		cluster, err := redigoCluster.NewCluster(
			&redigoCluster.Options{
				StartNodes:   p.redisHost.Addr,
				ConnTimeout:  time.Duration(p.redisHost.TimeoutMs) * time.Millisecond,
				ReadTimeout:  0,
				WriteTimeout: 0,
				KeepAlive:    16,
				AliveTime:    60 * time.Second,
				Password:     p.redisHost.Password,
			})
		if err == nil {
			p.conn = common.NewClusterConn(cluster, 0)
		}
	}
	if err != nil {
		return err
	}

	if len(p.redisHost.Password) != 0 {
		_, err = p.conn.Do(p.redisHost.Authtype, p.redisHost.Password)
		if err != nil {
			return err
		}
	}

	if p.redisHost.DBType != common.TypeCluster {
		_, err = p.conn.Do("select", p.Db)
		if err != nil {
			return err
		}
	}

	if p.conn == nil {
		return fmt.Errorf("connect host[%v] failed: unknown", p.redisHost.Addr)
	}
	return nil
}

func (p *RedisClient) Do(commandName string, args ...interface{}) (interface{}, error) {
	var err error
	var result interface{}
	for tryCount := 0; tryCount < common.MaxRetryCount; tryCount++ {
		if p.conn == nil {
			err = p.Connect()
			if err != nil {
				if p.CheckHandleNetError(err) {
					continue
				}
				return nil, err
			}
		}

		result, err = p.conn.Do(commandName, args...)
		if err != nil {
			if p.CheckHandleNetError(err) {
				continue
			}
			return nil, err
		}
		break
	} // end for {}
	return result, err
}

func (p *RedisClient) Close() {
	if p.conn != nil {
		p.conn.Close()
		p.conn = nil
	}
}

type combine struct {
	command string
	params  []interface{}
}

func (c combine) String() string {
	all := make([]string, 0, len(c.params)+1)
	all = append(all, c.command)
	for _, ele := range c.params {
		all = append(all, string(ele.([]byte)))
	}
	return strings.Join(all, " ")
}

func (p *RedisClient) Key(key *common.Key) interface{} {
	return append(p.redisHost.Prefix, key.Key...)
}

func (p *RedisClient) StrKey(key *common.Key) string {
	return string(append(p.redisHost.Prefix, key.Key...))
}

func (p *RedisClient) Params(key *common.Key, params []interface{}) []interface{} {
	list := make([]interface{}, len(params)+1)
	list[0] = p.Key(key)
	for i := 0; i < len(params); i++ {
		list[i+1] = params[i]
	}
	return list
}

func (p *RedisClient) Expire(key *common.Key, expire int64) bool {
	if ret, err := p.Do("expire", p.Key(key), expire); err != nil {
		return false
	} else {
		result := ret.(int64)
		return result > 0
	}
}

func (p *RedisClient) Get(key *common.Key) (interface{}, error) {
	return p.Do("get", p.Key(key))
}

func (p *RedisClient) Del(key *common.Key) (interface{}, error) {
	return p.Do("del", p.Key(key))
}

func (p *RedisClient) Set(key *common.Key, value interface{}) (interface{}, error) {
	if ret, err := p.Do("set", p.Key(key), value); err != nil {
		return nil, err
	} else {
		return ret, nil
	}
}

func (p *RedisClient) HashSet(key *common.Key, values []interface{}) (interface{}, error) {
	if ret, err := p.Do("hset", p.Params(key, values)...); err != nil {
		return nil, err
	} else {
		return ret, nil
	}
}

func (p *RedisClient) HashGet(key *common.Key, value interface{}) (interface{}, error) {
	if ret, err := p.Do("hget", p.Key(key), value); err != nil {
		return nil, err
	} else {
		return ret, nil
	}
}

func (p *RedisClient) SetAdd(key *common.Key, values []interface{}) (interface{}, error) {
	if ret, err := p.Do("sadd", p.Params(key, values)...); err != nil {
		return nil, err
	} else {
		return ret, nil
	}
}

func (p *RedisClient) ZsetAdd(key *common.Key, values []interface{}) (interface{}, error) {
	if ret, err := p.Do("zadd", p.Params(key, values)...); err != nil {
		return nil, err
	} else {
		return ret, nil
	}
}

func (p *RedisClient) ListPush(key *common.Key, values []interface{}) (interface{}, error) {
	if ret, err := p.Do("lpush", p.Params(key, values)...); err != nil {
		return nil, err
	} else {
		return ret, nil
	}
}

func (p *RedisClient) PipeRawCommand(commands []combine, specialErrorPrefix string) ([]interface{}, error) {
	if len(commands) == 0 {
		return nil, emptyError
	}

	result := make([]interface{}, len(commands))
	var err error
begin:
	for tryCount := 0; tryCount < common.MaxRetryCount; tryCount++ {
		if p.conn == nil {
			err = p.Connect()
			if err != nil {
				if p.CheckHandleNetError(err) {
					continue
				}
				common.Logger.Errorf("connect failed[%v]", err)
				return nil, err
			}
		}

		for _, ele := range commands {
			err = p.conn.Send(ele.command, ele.params...)
			if err != nil {
				if p.CheckHandleNetError(err) {
					continue begin
				}
				common.Logger.Errorf("send command[%v] failed[%v]", ele.command, err)
				return nil, err
			}
		}
		err = p.conn.Flush()
		if err != nil {
			if p.CheckHandleNetError(err) {
				continue
			}
			common.Logger.Errorf("flush failed[%v]", err)
			return nil, err
		}

		for i := 0; i < len(commands); i++ {
			reply, err := p.conn.Receive()
			if err != nil {
				if p.CheckHandleNetError(err) {
					continue begin
				}
				// 此处处理不太好，但是别人代码写死了，我只能这么改了
				if strings.HasPrefix(err.Error(), specialErrorPrefix) {
					// this error means the type between initial 'scan' and the following round comparison
					// is different. we should marks this.
					result[i] = common.TypeChanged
					continue
				}
				common.Logger.Errorf("receive command[%v] failed[%v]", commands[i], err)
				return nil, err
			}
			result[i] = reply
		}
		break
	} // end for {}
	return result, nil
}

func (p *RedisClient) PipeTypeCommand(keyInfo []*common.Key) ([]string, error) {
	commands := make([]combine, len(keyInfo))
	for i, key := range keyInfo {
		commands[i] = combine{
			command: "type",
			params:  []interface{}{p.Key(key)},
		}
	}

	result := make([]string, len(keyInfo))
	if ret, err := p.PipeRawCommand(commands, ""); err != nil {
		if err != emptyError {
			common.Logger.Errorf("run PipeRawCommand with commands[%v] failed[%v]", commands, err)
			return nil, err
		}
	} else {
		for i, ele := range ret {
			if v, ok := ele.(string); ok {
				result[i] = v
			} else {
				err := fmt.Errorf("run PipeRawCommand with commands[%s] return element[%v] isn't type string[%v]",
					printCombinList(commands), ele, reflect.TypeOf(ele))
				common.Logger.Error(err)
				return nil, err
			}
		}
	}
	return result, nil
}

func (p *RedisClient) PipeExistsCommand(keyInfo []*common.Key) ([]int64, error) {
	commands := make([]combine, len(keyInfo))
	for i, key := range keyInfo {
		commands[i] = combine{
			command: "exists",
			params:  []interface{}{p.Key(key)},
		}
	}
	return p.pipeInt64Result(keyInfo, commands, "")
}

func (p *RedisClient) PipeDelCommand(keyInfo []*common.Key) ([]int64, error) {
	commands := make([]combine, len(keyInfo))
	for i, key := range keyInfo {
		commands[i] = combine{
			command: "del",
			params:  []interface{}{p.Key(key)},
		}
	}
	return p.pipeInt64Result(keyInfo, commands, "")
}

func (p *RedisClient) PipeLenCommand(keyInfo []*common.Key) ([]int64, error) {
	commands := make([]combine, len(keyInfo))
	for i, key := range keyInfo {
		commands[i] = combine{
			command: key.Tp.FetchLenCommand,
			params:  []interface{}{p.Key(key)},
		}
	}
	return p.pipeInt64Result(keyInfo, commands, "WRONGTYPE")
}

func (p *RedisClient) PipeTTLCommand(keyInfo []*common.Key) ([]int64, error) {
	commands := make([]combine, len(keyInfo))
	for i, key := range keyInfo {
		commands[i] = combine{
			command: "ttl",
			params:  []interface{}{p.Key(key)},
		}
	}
	return p.pipeInt64Result(keyInfo, commands, "")
}

func (p *RedisClient) PipeValueCommand(keyInfo []*common.Key) ([]interface{}, error) {
	commands := make([]combine, len(keyInfo))
	for i, key := range keyInfo {
		switch key.Tp {
		case common.StringKeyType:
			commands[i] = combine{
				command: "get",
				params:  []interface{}{p.Key(key)},
			}
		case common.HashKeyType:
			commands[i] = combine{
				command: "hgetall",
				params:  []interface{}{p.Key(key)},
			}
		case common.ListKeyType:
			commands[i] = combine{
				command: "lrange",
				params:  []interface{}{p.Key(key), "0", "-1"},
			}
		case common.SetKeyType:
			commands[i] = combine{
				command: "smembers",
				params:  []interface{}{p.Key(key)},
			}
		case common.ZsetKeyType:
			commands[i] = combine{
				command: "zrange",
				params:  []interface{}{p.Key(key), "0", "-1", "WITHSCORES"},
			}
		default:
			commands[i] = combine{
				command: "get",
				params:  []interface{}{p.Key(key)},
			}
		}
	}

	if ret, err := p.PipeRawCommand(commands, ""); err != nil && err != emptyError {
		return nil, err
	} else {
		return ret, nil
	}
}

func (p *RedisClient) PipeSismemberCommand(key []byte, field [][]byte) ([]interface{}, error) {
	commands := make([]combine, len(field))
	for i, ele := range field {
		commands[i] = combine{
			command: "SISMEMBER",
			params:  []interface{}{key, ele},
		}
	}

	if ret, err := p.PipeRawCommand(commands, ""); err != nil && err != emptyError {
		return nil, err
	} else {
		return ret, nil
	}
}

func (p *RedisClient) PipeZscoreCommand(key []byte, field [][]byte) ([]interface{}, error) {
	commands := make([]combine, len(field))
	for i, ele := range field {
		commands[i] = combine{
			command: "ZSCORE",
			params:  []interface{}{key, ele},
		}
	}

	if ret, err := p.PipeRawCommand(commands, ""); err != nil && err != emptyError {
		return nil, err
	} else {
		return ret, nil
	}
}

func (p *RedisClient) FetchValueUseScan_Hash_Set_SortedSet(key *common.Key, onceScanCount int) (map[string][]byte, error) {
	var scanCmd string
	switch key.Tp {
	case common.HashKeyType:
		scanCmd = "hscan"
	case common.SetKeyType:
		scanCmd = "sscan"
	case common.ZsetKeyType:
		scanCmd = "zscan"
	default:
		return nil, fmt.Errorf("key type %s is not hash/set/zset", key.Tp)
	}
	cursor := 0
	value := make(map[string][]byte)
	for {
		reply, err := p.Do(scanCmd, p.Key(key), cursor, "count", onceScanCount)
		if err != nil {
			return nil, err
		}

		replyList, ok := reply.([]interface{})
		if ok == false || len(replyList) != 2 {
			return nil, fmt.Errorf("%s %s %d count %d failed, result: %+v", scanCmd, p.StrKey(key), cursor, onceScanCount, reply)
		}

		cursorBytes, ok := replyList[0].([]byte)
		if ok == false {
			return nil, fmt.Errorf("%s %s %d count %d failed, result: %+v", scanCmd, p.StrKey(key),
				cursor, onceScanCount, reply)
		}

		cursor, err = strconv.Atoi(string(cursorBytes))
		if err != nil {
			return nil, err
		}

		keylist, ok := replyList[1].([]interface{})
		if ok == false {
			panic(common.Logger.Criticalf("%s %s failed, result: %+v", scanCmd, p.StrKey(key), reply))
		}
		switch key.Tp {
		case common.HashKeyType:
			fallthrough
		case common.ZsetKeyType:
			for i := 0; i < len(keylist); i += 2 {
				value[string(keylist[i].([]byte))] = keylist[i+1].([]byte)
			}
		case common.SetKeyType:
			for i := 0; i < len(keylist); i++ {
				value[string(keylist[i].([]byte))] = nil
			}
		default:
			return nil, fmt.Errorf("key type %s is not hash/set/zset", key.Tp)
		}

		if cursor == 0 {
			break
		}
	} // end for{}
	return value, nil
}

func printCombinList(input []combine) string {
	ret := make([]string, 0, len(input))
	for _, ele := range input {
		ret = append(ret, ele.String())
	}
	return strings.Join(ret, "; ")
}

func (p *RedisClient) pipeInt64Result(keyInfo []*common.Key, commands []combine, specialErrorPrefix string) ([]int64, error) {
	result := make([]int64, len(keyInfo))
	if ret, err := p.PipeRawCommand(commands, specialErrorPrefix); err != nil {
		if err != emptyError {
			return nil, err
		}
	} else {
		for i, ele := range ret {
			if v, ok := ele.(int64); ok {
				result[i] = v
			} else {
				err := fmt.Errorf("run PipeRawCommand with commands[%s] return element[%v] isn't type int64[%v]",
					printCombinList(commands), ele, reflect.TypeOf(ele))
				common.Logger.Error(err)
				return nil, err
			}
		}
	}
	return result, nil
}

func (p *RedisClient) ToCommonKey(keyBytes []byte, opts ScanOpts) *common.Key {
	if len(p.redisHost.Prefix) > 0 && bytes.HasPrefix(keyBytes, p.redisHost.Prefix) {
		keyBytes = keyBytes[len(p.redisHost.Prefix):]
	}
	return &common.Key{
		Key:          keyBytes,
		Tp:           common.EndKeyType,
		ConflictType: common.EndConflict,
	}
}

func (p *RedisClient) ScanKeys(handler func([]*common.Key) (interface{}, error), opts ScanOpts) (int, error) {
	if len(opts.KeyList) > 0 {
		keysInfo := make([]*common.Key, 0, len(opts.KeyList))
		for _, value := range opts.KeyList {
			key := p.ToCommonKey([]byte(value), opts)
			if key != nil {
				keysInfo = append(keysInfo, key)
			}
		}
		_, err := handler(keysInfo)
		return len(keysInfo), err
	}
	cursor, total, times := 0, 0, 0
	if opts.Count <= 0 {
		opts.Count = 256
	}
	if len(p.redisHost.Prefix) > 0 {
		if len(opts.Match) > 0 {
			opts.Match = string(p.redisHost.Prefix) + opts.Match
		} else {
			opts.Match = string(p.redisHost.Prefix) + "*"
		}
	}
	emptyKey := make([]*common.Key, 0)
	var reply interface{}
	var err error
	for {
		times++
		reply, err = handler(emptyKey)
		params := []interface{}{"scan", cursor}
		if len(opts.Match) > 0 {
			params = append(params, "match", opts.Match)
		}
		params = append(params, "count", opts.Count)
		switch p.redisHost.DBType {
		case common.TypeAliyunProxy:
			params = append([]interface{}{"iscan", opts.Index}, params[1:])
		case common.TypeTencentProxy:
			params = append(params, opts.PhysicalDB)
		}
		if times%50 == 0 {
			common.Logger.Info(params)
		}
		reply, err = p.Do(params[0].(string), params[1:]...)
		if err != nil {
			panic(common.Logger.Critical(err))
		}
		replyList, ok := reply.([]interface{})
		if ok == false || len(replyList) != 2 {
			panic(common.Logger.Criticalf("scan %d count %d failed, result: %+v", cursor, opts.Count, reply))
		}
		cursorBytes, ok := replyList[0].([]byte)
		if ok == false {
			panic(common.Logger.Criticalf("scan %d count %d failed, result: %+v", cursor, opts.Count, reply))
		}
		cursor, err = strconv.Atoi(string(cursorBytes))
		if err != nil {
			panic(common.Logger.Critical(err))
		}
		keyList, ok := replyList[1].([]interface{})
		if ok == false {
			panic(common.Logger.Criticalf("scan failed, result: %+v", reply))
		}
		if len(keyList) > 0 {
			keysInfo := make([]*common.Key, 0, len(keyList))
			for _, value := range keyList {
				keyBytes, ok := value.([]byte)
				if ok == false {
					panic(common.Logger.Criticalf("scan failed, result: %+v", reply))
				}
				key := p.ToCommonKey(keyBytes, opts)
				if key != nil {
					keysInfo = append(keysInfo, key)
				}
			}
			reply, err = handler(keysInfo)
			if err != nil {
				panic(common.Logger.Criticalf("handle scan result failed: %+v", err.Error()))
			}
			total += len(keyList)
		}
		if cursor == 0 {
			break
		}
	}
	return total, nil

}

func (p *RedisClient) ScanValues(key *common.Key, batchCount int, handler func(*common.Key, interface{}) (interface{}, error)) (int, error) {
	totalCount := 0
	var getCmd, scanCmd, listCmd string
	switch key.Tp {
	case common.StringKeyType:
		getCmd = "get"
	case common.HashKeyType:
		scanCmd = "hscan"
	case common.SetKeyType:
		scanCmd = "sscan"
	case common.ZsetKeyType:
		scanCmd = "zscan"
	case common.ListKeyType:
		listCmd = "lrange"
	default:
		return totalCount, fmt.Errorf("key type %s is not string/hash/set/zset/list", key.Tp)
	}
	if getCmd != "" {
		reply, err := p.Do(getCmd, p.Key(key))
		if err != nil {
			return 0, err
		}
		reply, err = handler(key, reply)
		if err != nil {
			return 0, err
		}
		totalCount = 1
	} else if scanCmd != "" {
		cursor := 0
		for {
			reply, err := p.Do(scanCmd, p.Key(key), cursor, "count", batchCount)
			if err != nil {
				return totalCount, err
			}
			replyList, ok := reply.([]interface{})
			if ok == false || len(replyList) != 2 {
				return totalCount, fmt.Errorf("%s %s %d count %d failed, result: %+v", scanCmd, p.StrKey(key),
					cursor, batchCount, reply)
			}
			cursorBytes, ok := replyList[0].([]byte)
			if ok == false {
				return totalCount, fmt.Errorf("%s %s %d count %d failed, result: %+v", scanCmd, p.StrKey(key),
					cursor, batchCount, reply)
			}

			cursor, err = strconv.Atoi(string(cursorBytes))
			if err != nil {
				return totalCount, err
			}
			values, ok := replyList[1].([]interface{})
			if ok == false {
				panic(common.Logger.Criticalf("%s %s failed, result: %+v", scanCmd, p.StrKey(key), reply))
			}
			if len(values) > 0 {
				switch key.Tp {
				case common.HashKeyType:
					reply, err = handler(key, values)
					if err != nil {
						return 0, err
					}
					totalCount += len(values) / 2
				case common.ZsetKeyType:
					for i := 0; i < len(values); i += 2 { // score key => key score
						temp := values[i]
						values[i] = values[i+1]
						values[i+1] = temp
					}
					reply, err = handler(key, values)
					if err != nil {
						return 0, err
					}
					totalCount += len(values) / 2
				case common.SetKeyType:
					reply, err = handler(key, values)
					if err != nil {
						return 0, err
					}
					totalCount += len(values)
				}
			}
			if cursor == 0 {
				break
			}
		} // end for{}
	} else if listCmd != "" {
		for {
			reply, err := p.Do(listCmd, key.Key, totalCount, totalCount+batchCount-1)
			if err != nil {
				panic(common.Logger.Critical(err))
			}
			valueLen := len(reply.([]interface{}))
			if valueLen > 0 {
				reply, err = handler(key, reply)
				if err != nil {
					return 0, err
				}
				totalCount += valueLen
			}
			if valueLen < batchCount {
				break
			}
		} // end for{}
	}
	return totalCount, nil
}
