package common

import "reflect"

type Dynamic struct {
	value reflect.Value
}

func DynamicValue(o interface{}) *Dynamic {
	value := reflect.Indirect(reflect.ValueOf(o))
	return &Dynamic{value}
}

func (d *Dynamic) Int(key string, deft int) int {
	field := d.value.FieldByName(key)
	if !field.IsValid() {
		return deft
	}
	return int(field.Int())
}

func (d *Dynamic) Str(key string, deft string) string {
	field := d.value.FieldByName(key)
	if !field.IsValid() {
		return deft
	}
	return field.String()
}

func (d *Dynamic) Bool(key string, deft bool) bool {
	field := d.value.FieldByName(key)
	if !field.IsValid() {
		return deft
	}
	return field.Bool()
}
