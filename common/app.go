package common

import (
	"fmt"
	"os"
)

var VERSION = "V1.1.0"

type BaseOptions struct {
	LogFile  string `long:"log" description:"日志文件路径，未指定则输出到控制台"`
	LogLevel string `long:"log-level" description:"日志级别: debug，info，warn，error，默认：info"`
	Version  bool   `short:"v" long:"version" description:"显示当前版本信息"`
}

func AppRun(opt interface{}) (bool, error) {
	return RunWithArgs(opt, os.Args[1:])
}

func RunWithArgs(opt interface{}, args []string) (bool, error) {
	if ContainsAny(args, "-v", "--version") {
		fmt.Fprint(os.Stdout, VERSION)
		return true, nil
	}
	ret, err := ParseArgs(opt, args)
	if !ret || err != nil {
		return false, err
	}
	value := DynamicValue(opt)
	logLevel, err := HandleLogLevel(value.Str("LogLevel", "info"))
	if err != nil {
		return false, err
	}
	Logger, err = InitLog(value.Str("LogFile", ""), logLevel)
	if err != nil {
		return false, err
	}
	return true, nil
}
