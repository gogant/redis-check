package common

import "math/rand"

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandomStr(n int) string {
	str := make([]rune, n)
	total := len(letters)
	for i := range str {
		str[i] = letters[rand.Intn(total)]
	}
	return string(str)
}
