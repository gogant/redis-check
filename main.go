package main

import (
	"redis-check/command"
	"redis-check/common"
	"time"
)

type App struct {
	common.BaseOptions
	Preview command.PreviewCommand `command:"preview"`
	Test    command.TestCommand    `command:"test"`
	Compare command.CompareCommand `command:"compare"`
	Stat    command.StatCommand    `command:"stat"`
}

func main() {
	_, err := common.AppRun(&App{})
	if err != nil {
		panic(err.Error())
	}
	if common.Logger != nil && !common.Logger.Closed() {
		common.Logger.Close()
	}
	time.Sleep(time.Second)
}
