package handle

import (
	"redis-check/client"
	"redis-check/common"
	"redis-check/tool"
	"sync"
)

type KeyExistsComparator struct {
	CompareContext
}

func NewKeyExistsComparator(context CompareContext) *KeyExistsComparator {
	return &KeyExistsComparator{context}
}

func (p *KeyExistsComparator) FetchKeys(keyInfo []*common.Key, sourceClient *client.RedisClient, targetClient *client.RedisClient) {
	// fetch type
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		sourceKeyTypeStr, err := sourceClient.PipeTypeCommand(keyInfo)
		if err != nil {
			panic(common.Logger.Critical(err))
		}
		for i, t := range sourceKeyTypeStr {
			keyInfo[i].Tp = common.NewKeyType(t)
			keyInfo[i].SourceAttr.ItemCount = 1
		}
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		targetKeyTypeStr, err := targetClient.PipeExistsCommand(keyInfo)
		if err != nil {
			panic(common.Logger.Critical(err))
		}
		for i, t := range targetKeyTypeStr {
			keyInfo[i].TargetAttr.ItemCount = t
		}
		wg.Done()
	}()

	wg.Wait()
}

func (p *KeyExistsComparator) HandleCompare(keyInfo []*common.Key, conflictKey chan<- *common.Key, sourceClient *client.RedisClient, targetClient *client.RedisClient) {
	p.FetchKeys(keyInfo, sourceClient, targetClient)

	// re-mode ttl on the source side when key missing on the target side
	tool.RecheckTTL(keyInfo, sourceClient)

	// compare, filter
	for i := 0; i < len(keyInfo); i++ {
		// 在fetch type和之后的轮次扫描之间源端类型更改，不处理这种错误
		if keyInfo[i].SourceAttr.ItemCount == common.TypeChanged {
			continue
		}

		// key lack in target redis
		if keyInfo[i].TargetAttr.ItemCount == 0 &&
			keyInfo[i].TargetAttr.ItemCount != keyInfo[i].SourceAttr.ItemCount {
			keyInfo[i].ConflictType = common.LackTargetConflict
			p.IncrKeyStat(keyInfo[i])
			conflictKey <- keyInfo[i]
		}
	} // end of for i := 0; i < len(keyInfo); i++
}
