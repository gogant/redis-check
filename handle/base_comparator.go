package handle

import (
	"redis-check/client"
	"redis-check/common"
	"redis-check/metric"
)

type CompareContext struct {
	Stat       *metric.Stat
	BatchCount int
}

func (p *CompareContext) IncrKeyStat(oneKeyInfo *common.Key) {
	p.Stat.ConflictKey[oneKeyInfo.Tp.Index][oneKeyInfo.ConflictType].Inc(1)
}

func (p *CompareContext) IncrFieldStat(oneKeyInfo *common.Key, conType common.ConflictType) {
	p.Stat.ConflictField[oneKeyInfo.Tp.Index][conType].Inc(1)
}

type ICompareHandler interface {
	HandleCompare(keyInfo []*common.Key, conflictKey chan<- *common.Key, sourceClient *client.RedisClient,
		targetClient *client.RedisClient)
}
