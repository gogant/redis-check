# redis-check

#### 介绍
Redis的检查的工具，目前包含四个模块：
1.  预览：通过Scan模式扫描源Redis的key，可以根据Key的大小、匹配模式、在目标Redis是否存在进行过滤，对结果可以进行复制、删除等操作
2.  比对：比对源Redis和目的Redis的数据差异，可以基于值的长度、是否存在、值的内容进行比对，此模块基于阿里的 [redis_full_check](https://github.com/alibaba/RedisFullCheck) 进行的二次开发
3.  测试：对源Redis进行快速填充数据，模拟客户端对指定命令进行压力测试
4.  统计：解析Rdb文件，对Key进行分组统计数量、空间，占比，解析工具来源 [rdr](https://github.com/xueqiu/rdr)

#### 软件架构
工具使用子命令的模式进行交互，可通过 -h 查看帮助，目前支持四个子命令：compare-比较，preview-预览，test-测试，stat-统计，每个子命令也支持 -h 查看帮助信息

#### 安装教程
```
1.  下载工具：wget https://hello.zhaoping.fun/download/redis-check
2.  添加权限：chmod +x redis-check
3.  执行命令：sh redis-check command [option]
```

#### 使用说明
```
Application Options:
      --log=       日志文件路径，未指定则输出到控制台
      --log-level= 日志级别: 'debug'，'info'，'warn'，'error'，默认：'info'
  -v, --version    显示当前版本信息

Help Options:
  -h, --help       Show this help message

Available commands:
  compare
  preview
  stat
  test
```
#### 参与贡献