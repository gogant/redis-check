package conf

import (
	"fmt"
	"redis-check/client"
	"redis-check/common"
)

type RedisArgs struct {
	Addr         []string `long:"addr" description:"Redis地址，格式：host:port，多个使用分号分隔"`
	Password     string   `long:"pwd" description:"Redis密码"`
	AuthType     string   `long:"auth" default:"auth" regexp:"(auth)|(adminauth)" description:"Redis认证方式：auth/adminauth" `
	DBType       int      `long:"type" default:"0" regexp:"0|1" description:"Redis类型：0: 单机，1: 集群"`
	DBFilterList string   `long:"db" default:"0" description:"Redis DB列表，多个使用分号分隔"`
	Prefix       string   `long:"prefix" default:"" description:"Redis Key的前缀，获取到的key将会被移除此前缀"`
}

func (s *RedisArgs) ToRedisHost(role string, required bool) client.RedisHost {
	common.ScanArgs("请输入Redis信息", s, required, func() string {
		if len(s.Addr) == 0 || s.Addr[0] == "" {
			return "Redis地址不能为空"
		}
		addressList, err := client.HandleAddress(s.Addr[0], s.Password, s.AuthType)
		if err != nil {
			return fmt.Sprintf("Redis地址[%v]错误：%v", s.Addr, err)
		} else if len(addressList) == 0 {
			return "Redis地址不能为空"
		} else if len(addressList) > 1 && s.DBType != 1 {
			s.DBType = 1
		}
		s.Addr = addressList
		return ""
	})
	return client.RedisHost{
		Addr:         s.Addr,
		Password:     s.Password,
		TimeoutMs:    0,
		Role:         role,
		Authtype:     s.AuthType,
		DBType:       s.DBType,
		DBFilterList: common.FilterDBList(s.DBFilterList),
		Prefix:       []byte(s.Prefix),
	}
}
