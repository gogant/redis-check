module redis-check

go 1.15

require (
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/dongmx/rdb v0.0.0-20200714074246-e1191ecb6823
	github.com/garyburd/redigo v1.1.1-0.20170718224324-9e66b83d15a2
	github.com/jessevdk/go-flags v1.1.1-0.20161215105708-4e64e4a4e255
	github.com/jinzhu/copier v0.0.0-20190625015134-976e0346caa8
	github.com/logoove/sqlite v1.15.3
	github.com/vinllen/redis-go-cluster v1.0.1-0.20200120220710-4e63c4a1b59e
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	modernc.org/libc v1.15.1 // indirect
	modernc.org/mathutil v1.4.1
	modernc.org/sqlite v1.16.0 // indirect
)
